#!/bin/python

# This script creates a directory containing symlinks to the activated
# modules.
#
# Usage from bash:
# $~/electrans$ workon electrans
# $~/electrans$ export TRYTOND_CONFIG=trytond.conf
# $~/electrans$ export PYTHONPATH=trytond:proteus
# $~/electrans$ python3 remove_disabled_modules.py
#
# Para debugar desde vscode (no sé si es necesario lo anterior)
# (tryton-5.2) jaarias@vmTrytonStudio3:~/electrans$ code create_activated_modules_symlinks.py
# y luego F5
#
# By J.A. Arias (Electrans), on 04/10/2019

from proteus import config, Model, Wizard, Report
import os
import shutil

print('>> Conectando a BD tryton...')
config.set_trytond('postgresql:///electrans')

print('>> Abriendo modelo ir.module...')
Modules = Model.get('ir.module')
print('>> Modelo abierto. Buscar activados')
activated_modules = [m.name for m in Modules.find([('state','=','activated')])]

src = './trytond/trytond/modules'
dst = './trytond/trytond/disabled_modules'
modules_not_to_move = ['electrans_tools']

modules_in_src = [d for d in os.listdir(src) if os.path.isdir(os.path.join(src, d))]

for m in modules_in_src:
    if m not in activated_modules and m not in modules_not_to_move:
        from_ = os.path.join(src, m)
        to_ = os.path.join(dst, m)
        print('Moving ' + from_  + ' -> ' + to_)
        shutil.move(from_, to_)
