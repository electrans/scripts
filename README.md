Needed configuration to execute de scripts considering that our route is 'home/electrans/electrans/electrans_scripts":
	1. To allow to execute the scripts without need to specify the whole path every time or add this line in ~/.bashrc file:
		export PATH="$PATH:/home/electrans/electrans/electrans_scripts"
	2. Set environment variables used in most scripts:
		# route where the enviroment is stored
		export TRYTONROUTE='/home/user/electrans'
		# name of the virtualenv
		export TRYTONENV=electrans6
		# Set the default database used by scripts
		export DATABASE=electrans
		# Set route of trytond.conf
		export TRYTONDCONF='/home/user/electrans/trytond.conf'
                # Set route of access tokens for creating
                # pull requests from Bitbuckets API
                export TOKENS_ROUTE='/home/user/electrans/ACCESS_TOKENS.txt'
# prueba docker
