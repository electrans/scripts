#!/bin/bash
# Command used to start the tryton client
# echo 12345678 | xclip
source $VIRTUALENVWRAPPER_SCRIPT
workon $TRYTONENV
function help {
echo "Command used to start tryton client
t usage: t  [other options]
Options:
[-h] && [--help]is used to open this menu.
[other options] is used to pass arguments to trytond command. (OPTIONAL)"
exit
}
otherargs=''
for i in $*
do
if [[ $i = @("-h"|"--help") ]]
  then
    help
else
  otherargs+=" "$i
fi
done
clear
cowsay-r TRYTON CLIENT
$TRYTONROUTE/tryton/bin/tryton -vd $otherargs
deactivate
clear
